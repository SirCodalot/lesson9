#include "BSNode.h"
#include <iostream>

using namespace std;

int main()
{
	int intArr[15] = { 5, -2, 7, 4, 1, -7, 2, 9, -4, 3, 11, -10, -9, 14, -15};
	string strArr[15] = {
		"pizza",
		"soup",
		"ice cream",
		"apple",
		"sushi",
		"cheese",
		"pasta",
		"taco",
		"bacon",
		"chips",
		"fish",
		"chocolate",
		"salad",
		"chicken nuggets",
		"calamari"
	};

	for (int i = 0; i < 15; i++)
	{
		cout << intArr[i] << ", ";
	}
	cout << endl;

	for (int i = 0; i < 15; i++)
	{
		cout << strArr[i] << ", ";
	}
	cout << endl;

	BSNode<int>* intTree = new BSNode<int>(intArr[0]);
	BSNode<string>* strTree = new BSNode<string>(strArr[0]);

	for (int i = 1; i < 15; i++)
	{
		intTree->insert(intArr[i]);
		strTree->insert(strArr[i]);
	}
	
	cout << "Int Tree: " << endl;
	intTree->printNodes();

	cout << endl << "String Tree: " << endl;
	strTree->printNodes();

	cout << endl;

	system("pause");
	return 0;
}