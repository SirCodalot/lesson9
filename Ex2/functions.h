#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <iostream>

template <class T>
int compare(T a, T b)
{
	if (a < b)
	{
		return 1;
	}

	if (a == b)
	{
		return 0;
	}

	return -1;
}

template <class T>
void bubbleSort(T array[], int size)
{
	for (int current = size; size >= 0; size--)
	{
		for (int i = 0; i < current - 1; i++)
		{
			if (array[i + 1] < array[i])
			{
				T temp = array[i];
				array[i] = array[i + 1];
				array[i + 1] = temp;
			}
		}
	}
}

template <class T>
void printArray(T array[], int size)
{
	for (int i = 0; i < size; i++)
	{
		std::cout << array[i] << std::endl;
	}
}

#endif