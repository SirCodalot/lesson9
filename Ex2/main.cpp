#include "functions.h"
#include "tester.h"
#include <iostream>

int main() {

	std::cout << "DOUBLE TEST: " << std::endl;

	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<double>(1.0, 2.5) << std::endl;
	std::cout << compare<double>(4.5, 2.4) << std::endl;
	std::cout << compare<double>(4.4, 4.4) << std::endl;

	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	const int arr_size = 5;
	double doubleArr[arr_size] = { 1000.0, 2.0, 3.4, 17.0, 50.0 };
	bubbleSort<double>(doubleArr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << doubleArr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<double>(doubleArr, arr_size);
	std::cout << std::endl;

	std::cout << "FLOAT TEST: " << std::endl;

	// compare
	std::cout << "compare of 3.5 and 7.6: " << compare<float>(3.5f, 7.6f) << std::endl;

	// bubblesort & printarray
	float floatArr[4] = { 4.6f, 2.7f, 10.6f, 3.1f };
	bubbleSort<float>(floatArr, 4);

	std::cout << "bubblesort of 4.6, 2.7, 10.6, 3.1: " << std::endl;
	printArray<float>(floatArr, 4);


	std::cout << std::endl << "CHAR TEST: " << std::endl;

	// compare
	std::cout << "compare of 3.5 and 7.6: " << compare<float>(3.5f, 7.6f) << std::endl;

	// bubblesort & printarray
	char charArr[9] = { 'm', 'a', 'g', 's', 'h', 'i', 'm', 'i', 'm' };
	bubbleSort<char>(charArr, 9);

	std::cout << "bubblesort of \"Magshimim\": " << std::endl;
	printArray<char>(charArr, 9);


	std::cout << std::endl << "CUSTOM CLASS TEST: " << std::endl;

	// compare
	std::cout << "compare of 3 and 7: " << compare<Tester>(Tester(3), Tester(7)) << std::endl;

	// bubblesort & printarray
	Tester testArr[5] = { Tester(5), Tester(2), Tester(-7), Tester(0), Tester(8) };
	bubbleSort<Tester>(testArr, 5);

	std::cout << "bubblesort of 5, 2, -7, 0, 8: " << std::endl;
	printArray<Tester>(testArr, 5);

	system("pause");
	return 1;
}