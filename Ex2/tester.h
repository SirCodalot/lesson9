#ifndef TESTER_H
#define TESTER_H

#include <iostream>

using namespace std;

class Tester
{
public:
	int _info;

	Tester(int info);

	friend bool operator==(Tester& a, Tester& b);
	friend bool operator<(Tester& a, Tester& b);
	friend ostream& operator<<(ostream& a, Tester& b);
};

#endif