#include "tester.h"

Tester::Tester(int info)
{
	_info = info;
}

bool operator<(Tester& a, Tester& b)
{
	return a._info < b._info;
}

bool operator==(Tester& a, Tester& b)
{
	return a._info == b._info;
}

ostream& operator<<(ostream& a, Tester& b)
{
	a << "Tester of " << b._info;
	return a;
}
