#include "BSNode.h"

#include <iostream>

using namespace std;

BSNode::BSNode(string data)
{
	_data = data;
	_left = nullptr;
	_right = nullptr;
	_count = 1;
}

BSNode::BSNode(const BSNode& other)
{
	_data = other.getData();
	_left = other.getLeft() ? new BSNode(*other.getLeft()) : nullptr;
	_right = other.getRight() ? new BSNode(*other.getRight()) : nullptr;
	_count = other.getCount();
}

BSNode::~BSNode()
{
	delete _left;
	delete _right;
}

string BSNode::getData() const
{
	return _data;
}

int BSNode::getCount() const
{
	return _count;
}

BSNode* BSNode::getLeft() const
{
	return _left;
}

BSNode* BSNode::getRight() const
{
	return _right;
}

bool BSNode::isLeaf() const
{
	return !_left && !_right;
}

int BSNode::getHeight() const
{
	if (isLeaf())
	{
		return 0;
	}

	int right = 0;
	int left = 0;

	if (_right)
	{
		right = _right->getHeight();
	}

	if (_left)
	{
		left = _left->getHeight();
	}

	return 1 + (left < right ? right : left);
}

int BSNode::getDepth(const BSNode& root) const
{
	return getCurrNodeDistFromInputNode(&root);
}

int BSNode::getCurrNodeDistFromInputNode(const BSNode* node) const
{
	if (!node)
	{
		return -1;
	}

	if (_data == node->getData())
	{
		return 0;
	}
	
	if (_data < node->getData())
	{
		int dist = getCurrNodeDistFromInputNode(node->_left);
		return dist < 0 ? dist : 1 + dist;
	}

	if (_data > node->getData())
	{
		int dist = getCurrNodeDistFromInputNode(node->_right);
		return dist < 0 ? dist : 1 + dist;
	}
}

void BSNode::insert(string value)
{
	if (value == _data)
	{
		_count++;
		return;
	}

	if (value < _data)		// LEFT
	{
		if (_left)
		{
			_left->insert(value);
		}
		else
		{
			_left = new BSNode(value);
		}
	}
	else					// RIGHT
	{
		if (_right)
		{
			_right->insert(value);
		}
		else
		{
			_right = new BSNode(value);
		}
	}
}

bool BSNode::search(string val) const
{
	if (val == _data)
	{
		return true;
	}

	if (val < _data)
	{
		if (_left)
		{
			return _left->search(val);
		}
		else
		{
			return false;
		}
	}
	else
	{
		if (_right)
		{
			return _right->search(val);
		}
		else
		{
			return false;
		}
	}
}

void BSNode::printNodes() const
{
	if (_left)
	{
		_left->printNodes();
	}

	cout << _data << " " << _count << endl;

	if (_right)
	{
		_right->printNodes();
	}
}

BSNode& BSNode::operator=(const BSNode& other)
{
	_data = other.getData();
	_left = other.getLeft() ? new BSNode(*other.getLeft()) : nullptr;
	_right = other.getRight() ? new BSNode(*other.getRight()) : nullptr;
	_count = other.getCount();

	return *this;
}
