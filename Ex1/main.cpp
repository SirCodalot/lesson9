#include "BSNode.h"
#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>
#include "printTreeToFile.h"

using namespace std;

int main()
{
	BSNode* toPrint = new BSNode("juice");
	toPrint->insert("juice");
	toPrint->insert("pizza");
	toPrint->insert("pizza");
	toPrint->insert("apple");
	toPrint->insert("apple");
	toPrint->insert("sushi");
	toPrint->insert("sushi");
	toPrint->insert("soup");
	toPrint->insert("soup");

	cout << "tree with 5 couples of nodes:" << endl;
	toPrint->printNodes();
	cout << endl;

	BSNode* bs = new BSNode("6");
	bs->insert("2");
	bs->insert("8");
	bs->insert("3");
	bs->insert("5");
	bs->insert("9");
	bs->insert("6");
	
	for (int i = 0; i < 5; i++)
	{
		bs->insert("5");
	}

	BSNode* external = new BSNode("0");

	cout << "Tree height: " << bs->getHeight() << endl;
	cout << "depth of node with 5 depth: " << bs->getLeft()->getRight()->getRight()->getDepth(*bs) << endl;
	cout << "depth of node with 3 depth: " << bs->getLeft()->getRight()->getDepth(*bs) << endl;
	cout << "depth of non-existing node: " << external->getDepth(*bs) << endl;
	cout << "count of node with \"5\" is: " << bs->getLeft()->getRight()->getRight()->getCount() << endl;
	cout << "count of node with \"6\" is: " << bs->getCount() << endl;
	cout << "count of node with \"2\" is: " << bs->getLeft()->getCount() << endl;
	cout << "checking if node with \"5\" is a leaf: " << (bs->getLeft()->getRight()->getRight()->isLeaf() ? "leaf" : "not leaf") << endl;
	cout << "checking if node with \"2\" is a leaf: " << (bs->getLeft() ? "leaf" : "not leaf") << endl;
	cout << "searching for node with \"8\": " << (bs->search("8") ? "found" : "not found") << endl;
	cout << "searching for node with \"3\": " << (bs->search("3") ? "found" : "not found") << endl;
	cout << "searching for node with \"0\": " << (bs->search("0") ? "found" : "not found") << endl;
	
	string textTree = "BSTData.txt";
	printTreeToFile(bs, textTree);

	system("BinaryTree.exe");
	system("pause");
	remove(textTree.c_str());
	delete bs;

	return 0;
}

