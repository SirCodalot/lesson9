#ifndef BSNode_H
#define BSNode_H

#include <string>

using namespace std;

template<class T>
class BSNode
{
public:
	BSNode(T data)
	{
		_data = data;
		_left = nullptr;
		_right = nullptr;
		_count = 1;
	}
	BSNode(const BSNode& other)
	{
		_data = other.getData();
		_left = other.getLeft() ? new BSNode(*other.getLeft()) : nullptr;
		_right = other.getRight() ? new BSNode(*other.getRight()) : nullptr;
		_count = other.getCount();
	}

	~BSNode()
	{
		delete _left;
		delete _right;
	}

	void insert(T value)
	{
		if (value == _data)
		{
			_count++;
			return;
		}

		if (value < _data)		// LEFT
		{
			if (_left)
			{
				_left->insert(value);
			}
			else
			{
				_left = new BSNode(value);
			}
		}
		else					// RIGHT
		{
			if (_right)
			{
				_right->insert(value);
			}
			else
			{
				_right = new BSNode(value);
			}
		}
	}
	BSNode& operator=(const BSNode& other)
	{
		_data = other.getData();
		_left = other.getLeft() ? new BSNode(*other.getLeft()) : nullptr;
		_right = other.getRight() ? new BSNode(*other.getRight()) : nullptr;
		_count = other.getCount();

		return *this;
	}

	bool isLeaf() const
	{
		return !_left && !_right;
	}
	T getData() const
	{
		return _data;
	}
	int getCount() const
	{
		return _count;
	}
	BSNode* getLeft() const
	{
		return _left;
	}
	BSNode* getRight() const
	{
		return _right;
	}

	bool search(T val) const
	{
		if (val == _data)
		{
			return true;
		}

		if (val < _data)
		{
			if (_left)
			{
				return _left->search(val);
			}
			else
			{
				return false;
			}
		}
		else
		{
			if (_right)
			{
				return _right->search(val);
			}
			else
			{
				return false;
			}
		}
	}

	int getHeight() const
	{
		if (isLeaf())
		{
			return 0;
		}

		int right = 0;
		int left = 0;

		if (_right)
		{
			right = _right->getHeight();
		}

		if (_left)
		{
			left = _left->getHeight();
		}

		return 1 + (left < right ? right : left);
	}
	int getDepth(const BSNode& root) const
	{
		return getCurrNodeDistFromInputNode(&root);
	}

	void printNodes() const //for question 1 part C
	{
		if (_left)
		{
			_left->printNodes();
		}

		cout << _data << " " << _count << endl;

		if (_right)
		{
			_right->printNodes();
		}
	}

private:
	T _data;
	BSNode* _left;
	BSNode* _right;

	int _count; //for question 1 part B
	int getCurrNodeDistFromInputNode(const BSNode* node) const //auxiliary function for getDepth
	{
		if (!node)
		{
			return -1;
		}

		if (_data == node->getData())
		{
			return 0;
		}

		if (_data < node->getData())
		{
			int dist = getCurrNodeDistFromInputNode(node->_left);
			return dist < 0 ? dist : 1 + dist;
		}

		if (_data > node->getData())
		{
			int dist = getCurrNodeDistFromInputNode(node->_right);
			return dist < 0 ? dist : 1 + dist;
		}
	}
};

#endif